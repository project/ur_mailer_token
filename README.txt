User Relationship Mailer Token Module
-------------------------------------
This is a plugin module for the User Relationships module, based initially on
the included user_relationship_mailer module. The difference in this case is
the module uses token replacement patterns instead of the internal string
replacements and has support for Wysiwyg.

Fundamentally, it acts the same as user_relationship_mailer in that it provides
the facility to email users notifications about relationship changes.

Please post issues/feedback at http://drupal.org/project/1881168.


Requirements
------------
Drupal 6
User Relationships Module
PHP 5.3 (Only because I am assuming that as a minimum when writing code.)


Installation
------------
Enable User Relationship Mailer Token in the "Site building -> modules" administration screen.


IMPORTANT MIGRATION INFORMATION
-----------
This does not retain any settings from user_relationship_mailer. You will have
to enable/disable each email type, copy over your templates, etc. Also make
sure to disable the core UR Mailer module so you do not send out duplicates.

Credits
-------
Module fork with enhanced Token support by Robert Brown.
Token support initially by 63reasons http://drupal.org/node/442138
Updated by Alex Karshakevich http://drupal.org/user/183217
Written by Jeff Smick.
Written originally for and financially supported by OurChart Inc. (http://www.ourchart.com)
Thanks to the BuddyList module team for their inspiration
