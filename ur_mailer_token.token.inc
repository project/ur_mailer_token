<?php

/**
 * @file
 * Token integration functions for ur_mailer_token module.
 */

/**
 * Implements hook_token_values().
 */
function ur_mailer_token_token_values($type = 'all', $relationship = NULL, $options = array()) {
  // Contains the $user object for the requester and requestee
  $requester = $relationship->requester;
  $requestee = $relationship->requestee;

  if ($type == 'relationship' && is_object($relationship)) {
    // Requester Tokens
    if ($requester->uid) {
      $values['requester-uid'] = $requester->uid;
      $values['requester-name'] = $requester->name;
      $values['requester-link'] = theme('user_relationships_user_link', $requester->uid);
      $values['requester-list-link'] = url("user/{$requester->uid}/relationships/list", array('absolute' => TRUE));
      $values['requester-pending-link'] = url("user/{$requester->uid}/relationships/requests", array('absolute' => TRUE));
      $values = array_merge($values, ur_mailer_token_cp_lookup_values($requester, 'requester'));
    }
    // Requestee Tokens
    if ($requestee->uid) {
      $values['requestee-uid'] = $requestee->uid;
      $values['requestee-name'] = $requestee->name;
      $values['requestee-link'] = theme('user_relationships_user_link', $requestee->uid);
      $values['requestee-list-link'] = url("user/{$requestee->uid}/relationships/list", array('absolute' => TRUE));
      $values['requestee-pending-link'] = url("user/{$requestee->uid}/relationships/requests", array('absolute' => TRUE));
      $values = array_merge($values, ur_mailer_token_cp_lookup_values($requestee, 'requestee'));
    }
    // Relationship
    $values['relationship-name'] = $relationship->relationship_type->name;
    $values['relationship-plural-name'] = $relationship->relationship_type->plural_name;
    $values['profile-uid'] = $relationship->profile_uid;
    $values['target-name'] = $relationship->target_name;
    if ($deleted_by = $relationship->deleted_by) {
      $values['deleted-by-name'] = $deleted_by->name;
      $values['deleted-by-link'] = url("user/{$deleted_by->uid}", array('absolute' => TRUE));
      $values['deleted-by-uid']  = $deleted_by->uid;
    }
  }

  return $values;
}

/**
 * Helper function: Return content profile fields for a given user
 *
 * This is abstracted into its own function as we run through it for both
 * requester and requestee.
 */
function ur_mailer_token_cp_lookup_values($user, $user_type) {
  $values = array();

  if (module_exists('content_profile') && $user) {
    $content_profile_types = content_profile_get_types();
    $field_types = _content_field_types();

    // Load each defined content profile type
    foreach ($content_profile_types as $type => $details) {
      $profile = content_profile_load($type, $user->uid);
      if ($profile) {
        // Much of this is adapted directly from CCK content.token.inc.
        // Prevent against invalid 'nodes' built by broken 3rd party code.
        if (isset($profile->type)) {
          // Let PHP free the $node object when we are done. Working directly on the
          // incoming $object causes memory leak issues on long-running scripts such
          // as migrations. See http://drupal.org/node/736440.
          $node = drupal_clone($profile);
          $content_type = content_types($node->type);
          $node->build_mode = 'token';
          $node->content = array();
          content_view($node);
          // The formatted values will only be known after the content has been rendered.
          drupal_render($node->content);
          content_alter($node);
          // ImageCache support. Collect a list of all preset names here.
          if (module_exists('imagecache')) {
            $icpresets = imagecache_presets();
            foreach ($icpresets as $preset) {
              $presetnames[] = $preset['presetname'];
            }
          }

          foreach ($content_type['fields'] as $field_name => $field) {
            $items = isset($node->{$field_name}) ? $node->{$field_name} : array();
            $function = $field_types[$field['type']]['module'] . '_token_values';
            if (!empty($items) && function_exists($function)) {
              $token_values = (array) $function('field', $items, $options);
              foreach ($token_values as $token => $value) {
                $values[$user_type . '-' . $type . '-' . $field_name . '-' . $token] = $value;
              }
              // Add tokens for the URL of each defined ImageCache preset
              if (module_exists('imagecache') && $field['widget']['module'] == 'imagefield') {
                foreach ($presetnames as $presetname) {
                  $value = ltrim(imagecache_create_url($presetname, $token_values['filefield-filepath'], FALSE, FALSE), '/');
                  $values[$user_type . '-' . $type . '-' . $field_name . '-' . $presetname . '-filepath'] = $value;
                }
              }
            }
          }

          // Also collect information on the node itself. This is adapted from
          // token module, token_node.inc.
          $account = db_fetch_object(db_query("SELECT name, mail FROM {users} WHERE uid = %d", $node->uid));

          // Adjust for the anonymous user name.
          if (!$node->uid && !$account->name) {
            $account->name = variable_get('anonymous', t('Anonymous'));
          }

          $values[$user_type . '-' . $type . '-' . 'nid'] = $node->nid;
          $values[$user_type . '-' . $type . '-' . 'type'] = $node->type;
          $values[$user_type . '-' . $type . '-' . 'type-name'] = node_get_types('name', $node->type);
          $values[$user_type . '-' . $type . '-' . 'language'] = check_plain($node->language);
          $values[$user_type . '-' . $type . '-' . 'title'] = check_plain($node->title);
          $values[$user_type . '-' . $type . '-' . 'title-raw'] = $node->title;
          $values[$user_type . '-' . $type . '-' . 'node-path-raw'] = drupal_get_path_alias('node/'. $node->nid);
          $values[$user_type . '-' . $type . '-' . 'node-path'] = check_plain($values['node-path-raw']);
          $values[$user_type . '-' . $type . '-' . 'node-url'] = url('node/' . $node->nid, array('absolute' => TRUE));
          $values[$user_type . '-' . $type . '-' . 'author-uid'] = $node->uid;
          $values[$user_type . '-' . $type . '-' . 'author-name'] = check_plain($account->name);
          $values[$user_type . '-' . $type . '-' . 'author-name-raw'] = $account->name;
          $values[$user_type . '-' . $type . '-' . 'author-mail'] = check_plain($account->mail);
          $values[$user_type . '-' . $type . '-' . 'author-mail-raw'] = $account->mail;

          $values[$user_type . '-' . $type . '-' . 'log-raw'] = isset($node->log) ? $node->log : '';
          $values[$user_type . '-' . $type . '-' . 'log'] = filter_xss($values['log-raw']);

          if (module_exists('comment')) {
            $values[$user_type . '-' . $type . '-' . 'node_comment_count'] = isset($node->comment_count) ? $node->comment_count : 0;
            $values[$user_type . '-' . $type . '-' . 'unread_comment_count'] = comment_num_new($node->nid);
          }
          else {
            $values[$user_type . '-' . $type . '-' . 'node_comment_count'] = 0;
            $values[$user_type . '-' . $type . '-' . 'unread_comment_count'] = 0;
          }

          if (isset($node->created)) {
            $values += token_get_date_token_values($node->created, $user_type . '-' . $type . '-');
          }

          if (isset($node->changed)) {
            $values += token_get_date_token_values($node->changed, $user_type . '-' . $type . '-mod-');
          }

          // And now taxonomy, which is a bit more work. This code is adapted from
          // pathauto's handling code; it's intended for compatibility with it.
          if (module_exists('taxonomy') && !empty($node->taxonomy) && is_array($node->taxonomy)) {
            foreach ($node->taxonomy as $term) {
              $original_term = $term;
              if ((object)$term) {
                // With free-tagging it's somewhat hard to get the tid, vid, name values
                // Rather than duplicating taxonomy.module code here you should make sure your calling module
                // has a weight of at least 1 which will run after taxonomy has saved the data which allows us to
                // pull it out of the db here.
                if (!isset($term->name) || !isset($term->tid)) {
                  $vid = db_result(db_query_range("SELECT t.vid FROM {term_node} r INNER JOIN {term_data} t ON r.tid = t.tid INNER JOIN {vocabulary} v ON t.vid = v.vid WHERE r.nid = %d ORDER BY v.weight, t.weight, t.name", $node->nid, 0, 1));
                  if (!$vid) {
                    continue;
                  }
                  $term = db_fetch_object(db_query_range("SELECT t.tid, t.name FROM {term_data} t INNER JOIN {term_node} r ON r.tid = t.tid WHERE t.vid = %d AND r.vid = %d ORDER BY t.weight", $vid, $node->vid, 0, 1));
                  $term->vid = $vid;
                }

                // Ok, if we still don't have a term name maybe this is a pre-taxonomy submit node
                // So if it's a number we can get data from it
                if (!isset($term->name) && is_array($original_term)) {
                  $tid = array_shift($original_term);
                  if (is_numeric($tid)) {
                    $term = taxonomy_get_term($tid);
                  }
                }
                $values[$user_type . '-' . $type . '-' . 'term'] = check_plain($term->name);
                $values[$user_type . '-' . $type . '-' . 'term-raw'] = $term->name;
                $values[$user_type . '-' . $type . '-' . 'term-id'] = $term->tid;
                $vid = $term->vid;

                if (!empty($vid)) {
                  $vocabulary = taxonomy_vocabulary_load($vid);
                  $values[$user_type . '-' . $type . '-' . 'vocab'] = check_plain($vocabulary->name);
                  $values[$user_type . '-' . $type . '-' . 'vocab-raw'] = $vocabulary->name;
                  $values[$user_type . '-' . $type . '-' . 'vocab-id'] = $vocabulary->vid;
                }

                // The 'catpath' (and 'cat') tokens have been removed, as they caused quite a bit of confusion,
                // and the catpath was a relatively expensive query when the taxonomy tree was deep.
                //
                // It existed only to provide forward-compatability with pathauto module, and
                // for most uses of token.module, it was a relatively useless token -- it exposed
                // a list of term names formatted as a URL/path string. Once pathauto supports
                // tokens, *it* should handle this catpath alias as it's the primary consumer.
                break;
              }
            }
          }
          // It's possible to leave that block and still not have good data.
          // So, we test for these and if not set, set them.
          if (!isset($values[$user_type . '-' . $type . '-' . 'term'])) {
            $values[$user_type . '-' . $type . '-' . 'term'] = '';
            $values[$user_type . '-' . $type . '-' . 'term-raw'] = '';
            $values[$user_type . '-' . $type . '-' . 'term-id'] = '';
            $values[$user_type . '-' . $type . '-' . 'vocab'] = '';
            $values[$user_type . '-' . $type . '-' . 'vocab-raw'] = '';
            $values[$user_type . '-' . $type . '-' . 'vocab-id'] = '';
          }
        }
      }
    }
  }

  return $values;
}

/**
 * Implements hook_token_list().
 */
function ur_mailer_token_token_list($type = 'all') {
  if ($type == 'relationship' || $type == 'all') {
    $tokens = array();

    // Requester
    $tokens['Requester Tokens']['requester-uid']          = t('The requester uid.');
    $tokens['Requester Tokens']['requester-name']         = t('The requester name.');
    $tokens['Requester Tokens']['requester-link']         = t('The requester link.');
    $tokens['Requester Tokens']['requester-list-link']    = t('The requester list link.');
    $tokens['Requester Tokens']['requester-pending-link'] = t('The pending link of the requester.');
    $tokens = array_merge($tokens, ur_mailer_token_cp_lookup_list('requester'));

    // Requestee
    $tokens['Requestee Tokens']['requestee-uid']          = t('The requestee uid.');
    $tokens['Requestee Tokens']['requestee-name']         = t('The requestee name.');
    $tokens['Requestee Tokens']['requestee-link']         = t('The requestee link.');
    $tokens['Requestee Tokens']['requestee-list-link']    = t('The requestee list link.');
    $tokens['Requestee Tokens']['requestee-pending-link'] = t('The pending link of the requestee.');
    $tokens = array_merge($tokens, ur_mailer_token_cp_lookup_list('requestee'));

    // Relationship
    $tokens['Relationship']['relationship-name']          = t('Relationship name.');
    $tokens['Relationship']['relationship-plural-name']   = t('Relationship plural name.');
    $tokens['Relationship']['profile-uid']                = t('Profile UID.');
    $tokens['Relationship']['target-name']                = t('Target name.');
    $tokens['Relationship']['deleted-by-name']            = t('Deleted by name.');
    $tokens['Relationship']['deleted-by-link']            = t('Deleted by link.');
    $tokens['Relationship']['deleted-by-uid']             = t('Deleted by UID.');

    return $tokens;
  }
}

/**
 * Helper function: Return content profile field list
 */
function ur_mailer_token_cp_lookup_list($user_type) {
  $tokens = array();

  if ($user_type == 'requester') {
    $token_category = 'Requester Tokens';
  }
  elseif ($user_type == 'requestee') {
    $token_category = 'Requestee Tokens';
  }

  // Support for Content Profile module
  if (module_exists('content_profile')) {
    $content_profile_types = content_profile_get_types();
    $field_types = _content_field_types();
    // ImageCache support. Collect a list of all preset names here.
    if (module_exists('imagecache')) {
      $icpresets = imagecache_presets();
      foreach ($icpresets as $preset) {
        $presetnames[] = $preset['presetname'];
      }
    }

    // Load each defined content profile type
    foreach ($content_profile_types as $type => $details) {
      $content_type = content_types($type);
      foreach ($content_type['fields'] as $field_name => $field) {
        $sub_list = array();
        $function = $field_types[$field['type']]['module'] . '_token_list';
        if (function_exists($function)) {
          $sub_list = $function('field');
          foreach ($sub_list as $category => $token_list) {
            foreach ($token_list as $token => $description) {
              $tokens[$token_category][$user_type . '-' . $type . '-' . $field['field_name'] . '-' . $token] = $description;
            }
            // Support for ImageCache
            if (module_exists('imagecache') && $field['widget']['module'] == 'imagefield') {
              foreach ($presetnames as $presetname) {
                $tokens[$token_category][$user_type . '-' . $type . '-' . $field['field_name'] . '-' . $presetname . '-filepath'] = 'File path for ImageCache preset ' . $presetname;
              }
            }
          }
        }
      }

      // Adapted from node_token_list in token_node.inc from the Token module.
      $tokens[$token_category][$user_type . '-' . $type . '-nid'] = t('The unique ID of the content item, or "node".');
      $tokens[$token_category][$user_type . '-' . $type . '-type'] = t('The type of the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-type-name'] = t('The human-readable name of the node type.');
      $tokens[$token_category][$user_type . '-' . $type . '-language'] = t('The language the node is written in.');
      $tokens[$token_category][$user_type . '-' . $type . '-title'] = t('The title of the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-title-raw'] = t('The title of the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-node-path'] = t('The URL alias of the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-node-path-raw'] = t('The URL alias of the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-node-url'] = t('The URL of the node.');

      $tokens[$token_category][$user_type . '-' . $type . '-author-uid'] = t("The unique ID of the author of the node.");
      $tokens[$token_category][$user_type . '-' . $type . '-author-name'] = t("The login name of the author of the node.");
      $tokens[$token_category][$user_type . '-' . $type . '-author-name-raw'] = t("The login name of the author of the node.");
      $tokens[$token_category][$user_type . '-' . $type . '-author-mail'] = t("The email address of the author of the node.");
      $tokens[$token_category][$user_type . '-' . $type . '-author-mail-raw'] = t("The email address of the author of the node.");

      $tokens[$token_category][$user_type . '-' . $type . '-log'] = t('The explanation of the most recent changes made to the node.');
      $tokens[$token_category][$user_type . '-' . $type . '-log-raw'] = t('The explanation of the most recent changes made to the node.');

      $tokens[$token_category] += token_get_date_token_info(t('Node creation'), $user_type . '-' . $type . '-');
      $tokens[$token_category] += token_get_date_token_info(t('Node modification'), $user_type . '-' . $type . '-mod-');

      if (module_exists('comment')) {
        $tokens[$token_category][$user_type . '-' . $type . '-node_comment_count'] = t("The number of comments posted on a node.");
        $tokens[$token_category][$user_type . '-' . $type . '-unread_comment_count'] = t("The number of comments posted on a node since the reader last viewed it.");
      }

      if (module_exists('taxonomy')) {
        $tokens[$token_category][$user_type . '-' . $type . '-term'] = t("Name of top taxonomy term");
        $tokens[$token_category][$user_type . '-' . $type . '-term-raw'] = t("Unfiltered name of top taxonomy term.");
        $tokens[$token_category][$user_type . '-' . $type . '-term-id'] = t("ID of top taxonomy term");
        $tokens[$token_category][$user_type . '-' . $type . '-vocab'] = t("Name of top term's vocabulary");
        $tokens[$token_category][$user_type . '-' . $type . '-vocab-raw'] = t("Unfiltered name of top term's vocabulary.");
        $tokens[$token_category][$user_type . '-' . $type . '-vocab-id'] = t("ID of top term's vocabulary");
        // Temporarily disabled -- see notes in node_token_values.
        // $tokens[$token_category][$user_type . '-' . $type . '-catpath'] = t("Full taxonomy tree for the topmost term");
      }
    }
  }

  return $tokens;
}

/**
 * For a given context, builds a formatted list of tokens and descriptions
 * of their replacement values.
 *
 * @param type
 *   The token types to display documentation for. Defaults to 'all'.
 * @param prefix
 *   The prefix your module will use when parsing tokens. Defaults to '['
 * @param suffix
 *   The suffix your module will use when parsing tokens. Defaults to ']'
 * @return
 *   An HTML table containing the formatting docs.
 */
function theme_ur_mailer_token_token_help($type = 'all', $prefix = '[', $suffix = ']') {
  token_include();
  $full_list = array();
  foreach ((array)$type as $t) {
    $full_list = array_merge($full_list, token_get_list($t));
  }

  $headers = array(t('Token'), t('Replacement value'));
  $rows = array();
  foreach ($full_list as $key => $category) {
    $rows[] = array(array('data' => drupal_ucfirst($key) .' '. t('tokens'), 'class' => 'region', 'colspan' => 2));
    foreach ($category as $token => $description) {
      $row = array();
      $row[] = $prefix . $token . $suffix;
      $row[] = $description;
      $rows[] = $row;
    }
  }

  $output = theme('table', $headers, $rows, array('class' => 'description'));
  return $output;
}
