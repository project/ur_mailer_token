<?php

/**
 * @file
 * Admin forms for User Relationships Mailer Token module.
 */

/**
 * Menu callback; display invite settings form.
 */
function ur_mailer_token_settings() {
  $form = array();

  global $user;
  //take a sample relationship type
  $relationship = user_relationships_types_load();
  $relationship = array_pop($relationship);

  $form['mail'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Email Options'),
    '#weight' => -9,
  );
  $form['mail']['ur_mailer_token_send_mail'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Allow users to turn off relationship messages'),
    '#default_value'  => variable_get('ur_mailer_token_send_mail', TRUE),
    '#description'    => t('If you check this, users will have a new setting on their account edit page.'),
  );

  global $_ur_mailer_token_ops;

  $translations = _ur_mailer_token_ops_translations();
  foreach ($_ur_mailer_token_ops as $op) {
    $defaults_function = "ur_mailer_token_{$op}_default";
    $defaults = $defaults_function();

    $op_translated = isset($translations[$op]) ? $translations[$op] : t(ucfirst(str_replace('_', '-', $op)));
    $send_op_translated = isset($translations['send_'. $op]) ? $translations['send_'. $op] : t('Send @op messages', array('@op' => $op_translated));

    $form['mail'][$op] = array(
      '#type'         => 'fieldset',
      '#title'        => $op_translated,
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE
    );
    $form['mail'][$op]["ur_mailer_token_send_{$op}"] = array(
      '#type'           => 'checkbox',
      '#title'          => $send_op_translated,
      '#default_value'  => variable_get("ur_mailer_token_send_{$op}", TRUE),
    );
    $form['mail'][$op]["ur_mailer_token_{$op}_subject"] = array(
      '#type'           => 'textfield',
      '#title'          => t('Email subject'),
      '#maxlength'      => 500,
      '#size'           => 120,
      '#default_value'  => variable_get("ur_mailer_token_{$op}_subject", $defaults['subject']),
    );
    // Enable Wysiwyg for the textarea
    $form['mail'][$op]["ur_mailer_token_{$op}_message"] = array(
      '#type'           => 'item',
      '#tree'           => TRUE,
    );
    $message_defaults = variable_get("ur_mailer_token_{$op}_message", array('body' => $defaults['message'], 'format' => FILTER_FORMAT_DEFAULT));
    $form['mail'][$op]["ur_mailer_token_{$op}_message"]['body'] = array(
      '#type'           => 'textarea',
      '#title'          => t('Email message'),
      '#rows'           => 8,
      '#default_value'  => $message_defaults['body'],
    );
    $form['mail'][$op]["ur_mailer_token_{$op}_message"]['format'] = filter_form($message_defaults['format'], NULL, array("ur_mailer_token_{$op}_message", 'format'));
    $form['mail'][$op]['token_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['mail'][$op]['token_help']['tokens'] = array(
      '#value' => theme('ur_mailer_token_token_help', array('relationship', 'global')),
    );
  }

  return system_settings_form($form);
}
