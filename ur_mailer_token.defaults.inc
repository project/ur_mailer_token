<?php

/**
 * @file
 * User Relationships Mailer Token implementation.
 *
 * Contains all default strings and action definitions.
 */

global $_ur_mailer_token_ops;
$_ur_mailer_token_ops = array('request', 'cancel', 'approve', 'disapprove', 'remove', 'pre_approved');

/**
 * Translations of $_ur_mailer_token_ops above, used on the admin settings form.
 *  Keep the two in sync when adding new operations.
 *
 * @see http://drupal.org/node/515338
 */
function _ur_mailer_token_ops_translations() {
  return array(
    'request'       => t('Request'),
    'send_request'       => t('Send Request messages'),
    'cancel'        => t('Cancel'),
    'send_cancel'        => t('Send Cancel messages'),
    'approve'       => t('Approve'),
    'send_approve'       => t('Send Approve messages'),
    'disapprove'    => t('Disapprove'),
    'send_disapprove'    => t('Send Disapprove messages'),
    'remove'        => t('Remove'),
    'send_remove'        => t('Send Remove messages'),
    'pre_approved'  => t('Pre-approved'),
    'send_pre_approved'  => t('Send Pre-approved messages'),
  );
}

/**
 * Default request relationship message
 */
function ur_mailer_token_request_default() {
  $subject = "[site-name] [requester-name] wants to be your [relationship-name]";
  $message = <<<MESSAGE
Dear [requestee-name],

[requester-name] wants to be your [relationship-name].

Here's a link to [requester-name]'s profile:
  [requester-link]

To approve/disapprove this, log in to [site-url] and see your pending relationship requests at [requestee-pending-link].

Regards,
The [site-name] team
This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[requester-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}

/**
 * Default cancel request message
 */
function ur_mailer_token_cancel_default() {
  $subject = "[site-name] [requester-name] has canceled their [relationship-name] request";
  $message = <<<MESSAGE
Dear [requestee-name],

[requester-name] has canceled their [relationship-name] request.

Regards,
The [site-name] team

This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[requester-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}

/**
 * Default approve to relationship message
 */
function ur_mailer_token_approve_default() {
  $subject = "[site-name] You are [requestee-name]'s newest [relationship-name]";
  $message = <<<MESSAGE
Dear [requester-name],

[requestee-name] has approved your request to be their [relationship-name].

Here's a link to your relationships:
  [requester-list-link]

Regards,
The [site-name] team

This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[requester-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}

/**
 * Default disapprove to relationship message
 */
function ur_mailer_token_disapprove_default() {
  $subject = "[site-name] [requestee-name] has declined your relationship request";
  $message = <<<MESSAGE
Dear [requester-name],

[requestee-name] has declined your request to be their [relationship-name].

Regards,
The [site-name] team

This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[requester-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}

/**
 * Default removed relationship message
 */
function ur_mailer_token_remove_default() {
  $subject = "[site-name] [relationship-name] relationship between [requester-name] and [requestee-name] has been deleted";
  $message = <<<MESSAGE
Dear [target-name],

[requester-name]'s [relationship-name] relationship to [requestee-name] has been deleted by [deleted-by-name].

Regards,
The [site-name] team

This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[profile-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}

/**
 * Default no approval request relationship message
 */
function ur_mailer_token_pre_approved_default() {
  $subject = "[site-name] New [relationship-name] relationship between [requester-name] and [requestee-name]";
  $message = <<<MESSAGE
Dear [target-name],

We have just created a new [relationship-name] relationship between [requester-name] and [requestee-name].

Regards,
The [site-name] team

This message was sent to you because you have user relationships email notifications turned on.
If you no longer wish to receive these emails, you can turn them off at [site-url]/user/[profile-uid]/edit.
MESSAGE;

  return array(
    'subject' => $subject,
    'message' => $message,
  );
}
